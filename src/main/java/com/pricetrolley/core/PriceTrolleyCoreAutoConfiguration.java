package com.pricetrolley.core;

import com.pricetrolley.core.security.firebase.FirebaseAuthenticationProvider;
import com.pricetrolley.core.security.firebase.config.FirebaseProperties;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@AutoConfigureAfter({WebMvcAutoConfiguration.class})
@ComponentScan({
        "com.pricetrolley.core.security",
})
@EnableConfigurationProperties({FirebaseProperties.class})
public class PriceTrolleyCoreAutoConfiguration {
    private final FirebaseProperties firebaseProperties;

    public PriceTrolleyCoreAutoConfiguration(FirebaseProperties firebaseProperties) {
        this.firebaseProperties = firebaseProperties;
    }

    @Bean
    @ConditionalOnMissingBean
    public FirebaseAuthenticationProvider firebaseAuthenticationProvider() {
        return new FirebaseAuthenticationProvider();
    }
}
