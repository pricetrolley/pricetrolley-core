package com.pricetrolley.core.security;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.GrantedAuthority;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum Role implements GrantedAuthority {
    SUPER,
    ADMIN,
    EDITOR,
    USER;

    private final String description;

    Role() {
        this.description = StringUtils.capitalize(name().toLowerCase());
    }

    Role(String description) {
        this.description = description;
    }

    public static Role valueOf(GrantedAuthority authority) {
        String role = authority.getAuthority().replace("ROLE_", "");
        return valueOf(role);
    }

    public static Set<Role> parseSet(Set<String> stringRoles) {
        return stringRoles.stream().map(s -> Role.valueOf(s.trim().toUpperCase())).collect(Collectors.toSet());
    }

    public static Stream<Role> parse(Set<String> stringRoles) {
        return stringRoles.stream().map(s -> Role.valueOf(s.trim().toUpperCase()));
    }

    @Override
    public String getAuthority() {
        return String.format("ROLE_%s", this.name());
    }

    public String getDescription() {
        return description;
    }
}
