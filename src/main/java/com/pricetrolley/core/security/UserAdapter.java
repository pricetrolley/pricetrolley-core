package com.pricetrolley.core.security;

import com.google.common.collect.Sets;
import com.google.firebase.auth.UserRecord;
import com.pricetrolley.core.util.Nulls;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

public final class UserAdapter {

    private UserAdapter() {}

    public static UserDetails toUserDetails(UserRecord user) {
        Set<String> roles = new HashSet<>();
        user.getCustomClaims().forEach((s, o) -> roles.add(s.toUpperCase()));
        return toUserDetails(user.getUid(), user.isDisabled(), roles);
    }

    public static UserDetails toUserDetails(String id, boolean disabled, String... roles) {
        Set<String> stringSet = Sets.newHashSet(roles);
        return toUserDetails(id, disabled, stringSet);
    }

    public static UserDetails toUserDetails(String id, boolean disabled, Set<String> roles) {
        return AuthUser.withId(id)
                .username(id)
                .password(UUID.randomUUID().toString())
                .authorities(Role.parse(roles).collect(Collectors.toList()))
                .disabled(disabled)
                .build();
    }

    public static Optional<String> currentUserId() {
        Object principal = Nulls.ifNotNull(SecurityContextHolder.getContext().getAuthentication(), Authentication::getPrincipal);
        if (principal instanceof AuthUser) {
            return Optional.of(((AuthUser) principal).getId());
        }
        return Optional.empty();
    }
}
