package com.pricetrolley.core.security.firebase.config;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.io.InputStream;

@Configuration
public class FirebaseConfiguration {
    private static final Logger LOGGER = LoggerFactory.getLogger(FirebaseConfiguration.class);
    private final FirebaseProperties firebaseProperties;

    public FirebaseConfiguration(FirebaseProperties firebaseProperties) {
        this.firebaseProperties = firebaseProperties;
    }

    @Bean
    @Profile("gae")
    public FirebaseApp firebaseApp() throws IOException {
        LOGGER.info("Starting Firebase configuration in GAE env.");
        GoogleCredentials credentials = GoogleCredentials.getApplicationDefault();
        return initializeApp(credentials);
    }

    @Bean
    @Profile("!gae")
    public FirebaseApp localFirebaseApp(@Value("${com.pricetrolley.dev-credentials-file:/credentials.json}")
                                                    String devCredentialsFile) throws IOException {
        LOGGER.info("Starting Firebase configuration in Local env.");
        InputStream inputStream = new ClassPathResource(devCredentialsFile).getInputStream();
        GoogleCredentials credentials = GoogleCredentials.fromStream(inputStream);
        return initializeApp(credentials);
    }

    private FirebaseApp initializeApp(GoogleCredentials credentials) {
        FirebaseOptions options = (new FirebaseOptions.Builder())
                .setCredentials(credentials)
                .setDatabaseUrl(firebaseProperties.getDatabaseUrl())
                .build();

        if (FirebaseApp.getApps() == null || FirebaseApp.getApps().isEmpty()) {
            LOGGER.info("Creating FirebaseApp instance....");
            FirebaseApp.initializeApp(options);
        } else {
            LOGGER.info("Using existing FirebaseApp instance");
        }

        return FirebaseApp.getInstance();
    }
}
