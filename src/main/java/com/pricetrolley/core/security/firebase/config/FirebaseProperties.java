package com.pricetrolley.core.security.firebase.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("com.pricetrolley.firebase")
public class FirebaseProperties {

    /**
     * Firebase database URL
     */
    private String databaseUrl;

    public String getDatabaseUrl() {
        return databaseUrl;
    }

    public FirebaseProperties setDatabaseUrl(String databaseUrl) {
        this.databaseUrl = databaseUrl;
        return this;
    }
}
