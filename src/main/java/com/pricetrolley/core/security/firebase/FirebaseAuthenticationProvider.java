package com.pricetrolley.core.security.firebase;

import com.google.api.core.ApiFuture;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseToken;
import com.google.firebase.auth.UserRecord;
import com.pricetrolley.core.security.UserAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.session.SessionAuthenticationException;

import java.util.concurrent.ExecutionException;

public class FirebaseAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {
    private Logger LOG = LoggerFactory.getLogger(FirebaseAuthenticationProvider.class);

    public FirebaseAuthenticationProvider() {}

    @Override
    protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
        final FirebaseAuthenticationToken authenticationToken = (FirebaseAuthenticationToken) authentication;
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        ApiFuture<FirebaseToken> future = firebaseAuth.verifyIdTokenAsync(authenticationToken.getToken());
        try {
            final FirebaseToken token = future.get();
            UserRecord userRecord = firebaseAuth.getUserAsync(token.getUid()).get();
            return UserAdapter.toUserDetails(userRecord);
        } catch (InterruptedException | ExecutionException e) {
            throw new SessionAuthenticationException(e.getMessage());
        }
    }

    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken) throws AuthenticationException {}

    @Override
    public boolean supports(Class<?> authentication) {
        return FirebaseAuthenticationToken.class.isAssignableFrom(authentication);
    }
}
