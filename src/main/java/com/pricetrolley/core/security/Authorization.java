package com.pricetrolley.core.security;

public interface Authorization {
    String STAFF = "hasRole('EDITOR') or hasRole('ADMIN')";
}
