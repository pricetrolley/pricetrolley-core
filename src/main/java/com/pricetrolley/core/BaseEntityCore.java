package com.pricetrolley.core;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.googlecode.objectify.annotation.Ignore;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.OnSave;
import com.pricetrolley.core.util.OffsetDateTimeDeserializer;
import com.pricetrolley.core.util.OffsetDateTimeSerializer;

import java.time.OffsetDateTime;

/**
 * Created and updatedAt timestamps set. On creation updatedAt and createdAt are the same.
 * <p>
 */
public abstract class BaseEntityCore {

    /**
     * A marker interface to allow us to specify when to pre-fetch user Ref entities.
     * See https://github.com/objectify/objectify/wiki/BasicOperations#load-groups for how to do this.
     */
    public interface PreLoadAuditRefs {
    }

    /**
     * Optional hook for child entities
     */
    protected void onCreated() {
    }

    @Index
    @JsonSerialize(using = OffsetDateTimeSerializer.class)
    @JsonDeserialize(using = OffsetDateTimeDeserializer.class)
    private OffsetDateTime createdAt;

    @Index
    @JsonSerialize(using = OffsetDateTimeSerializer.class)
    @JsonDeserialize(using = OffsetDateTimeDeserializer.class)
    private OffsetDateTime updatedAt;

    /**
     * Handy for data migrations where you don't want to overwrite last updatedAt.
     */
    public void skipSettingAuditableFields() {
        skipSettingAuditableFields = true;
    }

    @Ignore
    private transient boolean skipSettingAuditableFields = false;

    public OffsetDateTime getCreatedAt() {
        return createdAt;
    }

    @JsonIgnore
    public boolean isSkipSettingAuditableFields() {
        return skipSettingAuditableFields;
    }

    public OffsetDateTime getUpdatedAt() {
        return updatedAt;
    }

    /**
     * Optional hook for child entities
     */
    protected void onUpdated() {
    }

    @OnSave
    private void setAuditableFieldsOnSave() {
        if (!skipSettingAuditableFields) {
            updatedAt = OffsetDateTime.now();
            onUpdated();

            if (createdAt == null) {
                onCreated();
                createdAt = updatedAt;
            }
        }
    }

    public static class Fields {
        public static final String updated = "updatedAt";
        public static final String created = "createdAt";
    }
}
