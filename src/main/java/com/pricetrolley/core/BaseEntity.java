package com.pricetrolley.core;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.appengine.api.datastore.Key;
import com.googlecode.objectify.annotation.Index;
import com.googlecode.objectify.annotation.Load;
import com.pricetrolley.core.security.UserAdapter;

import static com.google.appengine.api.datastore.KeyFactory.createKey;

public abstract class BaseEntity extends BaseEntityCore {
    public static final String USER_KIND = "User";

    // Only pre-load when specifically asked, using the marker interface
    @Load(PreLoadAuditRefs.class)
    @Index
    private Key createdBy;
    // Only pre-load when specifically asked, using the marker interface
    @Load(PreLoadAuditRefs.class)
    @Index
    private Key updatedBy;

    @JsonIgnore
    public String getCreatedBy() {
        return createdBy.getName();
    }

    @JsonIgnore
    public Key getCreatedByKey() {
        return createdBy;
    }

    @JsonIgnore
    public String getUpdatedBy() {
        return updatedBy.getName();
    }

    @JsonIgnore
    public Key getUpdatedByKey() {
        return updatedBy;
    }

    @Override
    protected void onCreated() {
        super.onCreated();
        createdBy = getCurrentUser();
    }

    @Override
    protected void onUpdated() {
        super.onUpdated();
        updatedBy = getCurrentUser();
    }

    private Key getCurrentUser() {
        return UserAdapter.currentUserId()
                .map(userId -> createKey(USER_KIND, userId))
                .orElse(null);
    }

    /**
     * A marker interface to allow us to specify when to pre-fetch user Ref entities.
     * See https://github.com/objectify/objectify/wiki/BasicOperations#load-groups for how to do this.
     */
    public interface PreLoadAuditRefs {
    }

    public static class Fields {
        public static final String updated = "updatedAt";
        public static final String updatedBy = "updatedBy";
        public static final String created = "createdAt";
        public static final String createdBy = "createdBy";
    }

}
