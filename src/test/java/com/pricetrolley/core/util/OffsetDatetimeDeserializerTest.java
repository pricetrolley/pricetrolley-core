package com.pricetrolley.core.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.junit.Test;

import java.io.IOException;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class OffsetDatetimeDeserializerTest {

    @Test
    public void testDeserializeShouldBeCorrect() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        //configure Object mapper for pretty print

        String actual = "{\"timestamp\":\"2017-11-01T11:11:11+0700\"}";
        TestEntity entity = objectMapper.readValue(actual, TestEntity.class);

        OffsetDateTime expected = OffsetDateTime.of(2017, 11, 1, 11, 11, 11, 0, ZoneOffset.ofHours(7));

        assertThat(entity.getTimestamp(), is(expected));
    }

    static class TestEntity {

        @JsonDeserialize(using = OffsetDateTimeDeserializer.class)
        private OffsetDateTime timestamp;

        public OffsetDateTime getTimestamp() {
            return timestamp;
        }

        public TestEntity setTimestamp(OffsetDateTime timestamp) {
            this.timestamp = timestamp;
            return this;
        }
    }

}
