package com.pricetrolley.core.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.junit.Test;

import java.io.IOException;
import java.io.StringWriter;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class OffsetDatetimeSerializerTest {

    @Test
    public void testSerializeShouldBeCorrect() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        //configure Object mapper for pretty print

        TestEntity entity = new TestEntity();
        OffsetDateTime time = OffsetDateTime.of(2017, 11, 1, 11, 11, 11, 10, ZoneOffset.ofHours(7));
        entity.setTimestamp(time);

        //writing to console, can write to any output stream such as file
        StringWriter stringEmp = new StringWriter();
        objectMapper.writeValue(stringEmp, entity);

        String expected = "{\"timestamp\":\"2017-11-01T11:11:11+0700\"}";
        assertThat(stringEmp.toString(), is(expected));
    }

    static class TestEntity {

        @JsonSerialize(using = OffsetDateTimeSerializer.class)
        private OffsetDateTime timestamp;

        public OffsetDateTime getTimestamp() {
            return timestamp;
        }

        public TestEntity setTimestamp(OffsetDateTime timestamp) {
            this.timestamp = timestamp;
            return this;
        }
    }

}
