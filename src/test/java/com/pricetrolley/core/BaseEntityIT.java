package com.pricetrolley.core;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.pricetrolley.core.testinfra.BaseIntegrationTest;
import com.pricetrolley.core.testinfra.context.WithMockPriceTrolleyUser;
import com.pricetrolley.core.testinfra.rules.LocalServicesRule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

public class BaseEntityIT extends BaseIntegrationTest {

    @Rule
    public LocalServicesRule localServicesRule = new LocalServicesRule();

    @Before
    public void setup() {
        localServicesRule.registerAdditionalEntities(TestBaseEntity.class);
    }

    @Test
    @WithMockPriceTrolleyUser
    public void testEntityShouldHaveCreatedBy() {
        TestBaseEntity entity = save(new TestBaseEntity());

        assertThat(entity.getCreatedByKey()).isNotNull();
        assertThat(entity.getCreatedBy()).isNotNull().isEqualTo("Tony");
        assertThat(entity.getUpdatedByKey()).isNotNull();
        assertThat(entity.getUpdatedBy()).isNotNull().isEqualTo("Tony");
    }

    @Entity
    private static class TestBaseEntity extends BaseEntity {
        @Id private String id;

        TestBaseEntity() {
            this.id = UUID.randomUUID().toString();
        }
    }
}
