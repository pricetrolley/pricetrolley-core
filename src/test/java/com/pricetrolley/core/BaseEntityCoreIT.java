package com.pricetrolley.core;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.pricetrolley.core.testinfra.BaseIntegrationTest;
import com.pricetrolley.core.testinfra.context.WithMockPriceTrolleyUser;
import com.pricetrolley.core.testinfra.rules.LocalServicesRule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

public class BaseEntityCoreIT extends BaseIntegrationTest {

    @Rule
    public LocalServicesRule localServicesRule = new LocalServicesRule();

    @Before
    public void setup() {
        localServicesRule.registerAdditionalEntities(TestBaseEntityCore.class);
    }

    @Test
    @WithMockPriceTrolleyUser
    public void testEntityShouldHaveCreatedBy() {
        TestBaseEntityCore entity = save(new TestBaseEntityCore());

        assertThat(entity.getCreatedAt()).isNotNull();
        assertThat(entity.getUpdatedAt()).isNotNull();
    }

    @Entity
    private static class TestBaseEntityCore extends BaseEntityCore {
        @Id private String id;

        TestBaseEntityCore() {
            this.id = UUID.randomUUID().toString();
        }
    }
}
