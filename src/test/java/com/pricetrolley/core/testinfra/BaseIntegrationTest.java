package com.pricetrolley.core.testinfra;

import com.pricetrolley.core.testinfra.rules.LocalServicesRule;
import org.junit.Rule;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static com.googlecode.objectify.ObjectifyService.ofy;

@RunWith(SpringRunner.class)
@TestPropertySource("classpath:application.properties")
@ContextConfiguration(classes = TestApplicationContext.class)
@SpringBootTest
public abstract class BaseIntegrationTest {

    @Rule
    public final ExpectedException thrown = ExpectedException.none();
    @Rule
    public LocalServicesRule localServicesRule = new LocalServicesRule();

    @SuppressWarnings("unchecked")
    protected <E> E save(E entity) {
        ofy().save().entities(entity).now();
        return entity;
    }

    protected <E> E[] save(E... entity) {
        ofy().save().entities(entity).now();
        return entity;
    }

    protected <E> void delete(E entity) {
        ofy().delete().entities(entity).now();
    }

    protected <E> void delete(E... entity) {
        ofy().delete().entities(entity).now();
    }
}
