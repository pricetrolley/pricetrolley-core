package com.pricetrolley.core.testinfra.rules;

import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.util.Closeable;
import org.junit.rules.ExternalResource;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LocalServicesRule extends ExternalResource {
    private LocalServiceTestHelper helper;
    private Closeable ofyService;
    private List<Class<?>> entities;

    public LocalServicesRule(Class<?>... entities) {
        this.entities = Stream.of(entities).collect(Collectors.toList());
    }

    public void registerAdditionalEntities(Class<?>... entities) {
        for (Class<?> entity : entities) {
            ObjectifyService.register(entity);
            this.entities.add(entity);
        }
    }

    @Override
    protected void before() {
        helper = createLocalServiceTestHelper();
        helper.setUp();
        ofyService = ObjectifyService.begin();

        for (Class<?> entity : entities) {
            ObjectifyService.register(entity);
        }
    }

    @Override
    protected void after() {
        helper.tearDown();
        ofyService.close();
    }

    private LocalServiceTestHelper createLocalServiceTestHelper() {
        return new LocalServiceTestHelper(
                new LocalDatastoreServiceTestConfig().setApplyAllHighRepJobPolicy()
        );
    }
}
