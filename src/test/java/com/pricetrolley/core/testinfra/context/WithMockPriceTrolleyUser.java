package com.pricetrolley.core.testinfra.context;

import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithSecurityContext;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
@WithSecurityContext(factory = WithMockPriceTrolleyUserSecurityContextFactory.class)
@WithMockUser
public @interface WithMockPriceTrolleyUser {

    String username() default "Tony";

    String name() default "Tony Stark";

    String[] roles() default {"USER"};

    String[] authorities() default {};
}