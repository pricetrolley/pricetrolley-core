package com.pricetrolley.core.testinfra.context;

import com.pricetrolley.core.security.UserAdapter;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.test.context.support.WithSecurityContextFactory;

public class WithMockPriceTrolleyUserSecurityContextFactory implements WithSecurityContextFactory<WithMockPriceTrolleyUser> {
    @Override
    public SecurityContext createSecurityContext(WithMockPriceTrolleyUser mockUser) {
        SecurityContext context = SecurityContextHolder.createEmptyContext();
        UserDetails authUser = UserAdapter.toUserDetails(mockUser.username(), false, mockUser.roles());
        Authentication authentication = new UsernamePasswordAuthenticationToken(authUser, authUser.getPassword(), authUser.getAuthorities());
        context.setAuthentication(authentication);
        return context;
    }
}
