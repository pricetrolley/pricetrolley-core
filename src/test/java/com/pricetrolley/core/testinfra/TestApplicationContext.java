package com.pricetrolley.core.testinfra;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(TestApplication.class)
public class TestApplicationContext {
}
