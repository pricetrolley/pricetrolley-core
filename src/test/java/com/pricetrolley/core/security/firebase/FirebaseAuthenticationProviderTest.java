package com.pricetrolley.core.security.firebase;

import com.pricetrolley.core.testinfra.TestApplicationContext;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = TestApplicationContext.class)
@SpringBootTest
public class FirebaseAuthenticationProviderTest {
    @Autowired
    private FirebaseAuthenticationProvider provider;

    @Test
    public void firebaseAuthenticationProvider_shouldBeCreated() {
        assertThat(provider).isNotNull();
    }
}
