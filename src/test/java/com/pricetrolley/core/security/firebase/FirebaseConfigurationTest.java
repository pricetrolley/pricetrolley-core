package com.pricetrolley.core.security.firebase;

import com.google.firebase.FirebaseApp;
import com.pricetrolley.core.security.firebase.config.FirebaseConfiguration;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@TestPropertySource("classpath:application.properties")
@SpringBootTest(classes = {FirebasePropertiesTest.TestConfiguration.class, FirebaseConfiguration.class})
public class FirebaseConfigurationTest {

    @Autowired
    private FirebaseApp firebaseApp;

    @Test
    public void firebaseApp_shouldBeCreated() {
        assertThat(firebaseApp).isNotNull();
    }
}
