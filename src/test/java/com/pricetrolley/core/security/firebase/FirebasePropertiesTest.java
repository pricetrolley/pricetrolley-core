package com.pricetrolley.core.security.firebase;

import com.pricetrolley.core.security.firebase.config.FirebaseProperties;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {FirebasePropertiesTest.TestConfiguration.class})
public class FirebasePropertiesTest {
    @Autowired
    private FirebaseProperties properties;

    @Test
    public void should_Populate_MyConfigurationProperties() {
        assertThat(properties.getDatabaseUrl()).isEqualTo("https://pricetrolley.firebaseapp.com");
    }

    @EnableConfigurationProperties(FirebaseProperties.class)
    public static class TestConfiguration {
        // nothing
    }
}
