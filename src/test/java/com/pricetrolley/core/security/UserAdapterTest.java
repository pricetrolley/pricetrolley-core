package com.pricetrolley.core.security;

import com.pricetrolley.core.testinfra.TestApplicationContext;
import com.pricetrolley.core.testinfra.context.WithMockPriceTrolleyUser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = TestApplicationContext.class)
@SpringBootTest
public class UserAdapterTest {

    @Test
    public void testToUserDetailsByStringShouldBeInstanceOfAuthUser() {
        UserDetails user = UserAdapter.toUserDetails("id", true, "USER");
        assertThat(user).isInstanceOf(AuthUser.class);
    }

    @Test
    public void testGetCurrentIdShouldNotBePresent() {
        Optional<String> optional = UserAdapter.currentUserId();
        assertThat(optional).isNotPresent();
    }

    @Test
    @WithMockPriceTrolleyUser(username = "tony")
    public void testGetCurrentIdShouldBePresent() {
        Optional<String> optional = UserAdapter.currentUserId();
        assertThat(optional).isPresent().hasValue("tony");
    }
}
