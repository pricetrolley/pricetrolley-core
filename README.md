# PriceTrolley Core

## Latest

Maven

```xml
<dependency>
  <groupId>com.pricetrolley</groupId>
  <artifactId>core</artifactId>
  <version>0.1.9</version>
</dependency>
```

You will need to support maven repository

```xml
<repositories>
  <repository>
    <id>jcenter-snapshots</id>
    <name>jcenter</name>
    <url>https://jcenter.bintray.com/</url>
  </repository>
</repositories>
```

## Requirements
- Java 8+
- Appengine Standard Java 8+
- Spring boot 2.x.x

## Getting Started

## Must

You will need to have `credentials.json` for local.

You can copy the file from `src/test/resources/credentials.json` or create a new one from Google Cloud

### Spring Security

Provide 3 classes to config Spring Security.

- `FirebaseAuthenticationProvider.class`
- `FirebaseAuthenticationToken.class`
- `FirebaseAuthenticationTokenFilter.class`

### Firebase

#### Setup
    ...
    @SpringBootApplication
    @Import(FirebaseConfiguration.class)
    public class Application {
    ...

To config Firebase Database URL add the following to your spring configuration

```
com.pricetrolley.core.security.firebase.database-url=<databaseUrl>
```

How to setup Spring Security config - see [example](README-SPRING-SECURITY.md). 

## Development

### Run Integration Test

`mvn verify -B`

### Deploy

`mvn -V -B -s settings.xml package deploy`
